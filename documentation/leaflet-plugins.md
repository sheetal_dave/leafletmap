# Leaflet Plugins used in this project
- leaflet.fullscreen => https://github.com/brunob/leaflet.fullscreen

- Leaflet.PolylineMeasure => https://github.com/ppete2/Leaflet.PolylineMeasure

- SearchWithSidebar => https://github.com/bung87/leaflet-searchbox

- GPS Locator => https://github.com/domoritz/leaflet-locatecontrol

- print Map => https://www.npmjs.com/package/leaflet.browser.print

- leaflet-routing => https://github.com/perliedman/leaflet-routing-machine
