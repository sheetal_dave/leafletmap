import {Component, OnInit, ViewChild, ElementRef, AfterViewInit} from '@angular/core';

import * as L from 'leaflet';

// plugins JS
import '../../node_modules/leaflet.fullscreen/Control.FullScreen.js';
import '../../node_modules/leaflet.polylinemeasure/Leaflet.PolylineMeasure.js';
import '../../node_modules/leaflet-searchbox/src/index.js';
import '../../node_modules/leaflet.locatecontrol/src/L.Control.Locate.js';
import '../../node_modules/leaflet.browser.print/dist/leaflet.browser.print.min.js';
import '../../node_modules/leaflet-routing-machine/dist/leaflet-routing-machine.js';
import '../../node_modules/leaflet-routing-machine/examples/Control.Geocoder.js';

import {tileLayer} from 'leaflet';

// Marker configurations
const iconRetinaUrl = 'assets/marker-icon-2x.png';
const iconUrl = 'assets/marker-icon.png';
const shadowUrl = 'assets/marker-shadow.png';
const iconDefault = L.icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});
L.Marker.prototype.options.icon = iconDefault;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, AfterViewInit {

  private map: L.Map;
  @ViewChild('map')
  private mapContainer: ElementRef<HTMLElement>;
  destBtn: any;
  startBtn: any;
  currentLocation;

  ngAfterViewInit(): void {
    // initialize map (with any location)
    this.map = new L.Map(this.mapContainer.nativeElement).setView(
      [21.1702, 72.8311], 5);

    // add tilesLayer to the map
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      {
        maxZoom: 20,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      }).addTo(this.map);


    // add locator (GPS)
    L.control.locate({
      position: 'topright',
      cacheLocation: true,
      drawMarker: true,
      showPopup: true,
      drawCircle: true,
      setView: true,
      locateOptions: {
        maxZoom: 25
      },
      strings: {
        title: 'Show me where I am, yo!'
      }
    }).addTo(this.map);

    // add a scale to the map
    L.control.scale().addTo(this.map);

    // add search box with sidebar
    const control = L.control.searchbox({
      // remove these if dont need sidebar
      sidebarTitleText: 'Header',
      sidebarMenuItems: {
        Items: [
          {type: 'link', name: 'Link 1 (github.com)', href: 'http://github.com', icon: 'icon-local-carwash'},
          {type: 'link', name: 'Link 2 (google.com)', href: 'http://google.com', icon: 'icon-cloudy'},
          {type: 'button', name: 'Button 1', onclick: 'alert(\'button 1 clicked !\')', icon: 'icon-potrait'},
          {type: 'button', name: 'Button 2', onclick: 'button2_click();', icon: 'icon-local-dining'},
          {type: 'link', name: 'Link 3 (stackoverflow.com)', href: 'http://stackoverflow.com', icon: 'icon-bike'},

        ]
      }
    });

    this.map.addControl(control);
    let searchedLocation;
    this.map.on('geosearch/showlocation', (e: any) => {
      const searchInput: any = document.getElementsByClassName('leaflet-searchbox-control-input')[0];
      searchInput.value = e.location.display_name;
      searchedLocation = e;
      this.map.setZoom(10);
      L.marker(e.location.latlng, {
        icon: new L.Icon({
          iconUrl,
          iconRetinaUrl,
          shadowUrl,
          className: 'geosearch-location'
        }), draggable: false
      }).addTo(this.map);
    });

    // display popup with searched location
    if (searchedLocation) {
      this.map.on('contextmenu', (event) => {
        L.popup()
          .setLatLng(searchedLocation && searchedLocation.location.latlng)
          .setContent(searchedLocation && searchedLocation.location.display_name)
          .addTo(this.map)
          .openOn(this.map);
      });
    }

    // measure control on map to draw lines between different points(locations) and calculate distance
    const polylineMeasure = L.control.polylineMeasure(
      {
        position: 'topright',
        unit: 'metres',
        showBearings: true,
        clearMeasurementsOnStop: false,
        showMeasurementsClearControl: true,
        showUnitControl: true
      });
    polylineMeasure.addTo(this.map);

    // create a fullscreen button and add it to the map
    L.control.fullscreen({
      position: 'topright',
      title: 'Show Map Fullscreen',
      titleCancel: 'Exit Fullscreen',
      content: null,
      forceSeparateButton: true,
      forcePseudoFullscreen: false,
      fullscreenElement: false
    }).addTo(this.map);

    this.map.on('enterFullscreen', () => this.map.invalidateSize());
    this.map.on('exitFullscreen', () => this.map.invalidateSize());

    // change the position of zoom controls (topleft' | 'topright' | 'bottomleft' | 'bottomright)
    this.map.zoomControl.setPosition('topright');

    // add different layers on map
    const layers = {
      baseLayers: {
        'Open Street Map': tileLayer(
          'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
          {
            subdomains: ['a', 'b', 'c'],
            maxZoom: 15,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          }
        ),
        'Open Cycle Map': tileLayer(
          'http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png',
          {
            subdomains: ['a', 'b', 'c'],
            maxZoom: 20,
            attribution: '&copy; <a href="http://www.opencyclemap.org/copyright">OpenCycleMap</a> contributors - &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          })
      }
    };
    L.control.layers(layers.baseLayers, null).addTo(this.map);

    // get current location of the user and create marker
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(data => {
        console.log(data);
        L.marker([data.coords.latitude, data.coords.longitude]).addTo(this.map);
        this.currentLocation.lat = data.coords.latitude;
        this.startBtn.coords.latitude.lng = data.coords.latitude;
        // adding popups to the map
        L.popup()
          .setLatLng([data.coords.latitude, data.coords.longitude])
          .setContent('Here I am !!')
          .openOn(this.map);
      });
    } else {
      alert('Geolocation is not supported in your browser');
    }

    // add print control on map
    L.control.browserPrint({position: 'topright'}).addTo(this.map);

    // @ts-ignore
    let geocoder = L.Control.Geocoder.nominatim();
    if (URLSearchParams && location.search) {
      // parse /?geocoder=nominatim from URL
      const params = new URLSearchParams(location.search);
      const geocoderString = params.get('geocoder');
      // @ts-ignore
      if (geocoderString && L.Control.Geocoder[geocoderString]) {
        console.log('Using geocoder', geocoderString);
        // @ts-ignore
        geocoder = L.Control.Geocoder[geocoderString]();
      } else if (geocoderString) {
        console.warn('Unsupported geocoder', geocoderString);
      }
    }

    // @ts-ignore
    L.Routing.control({
      waypoints: [
        L.latLng(20.5937, 78.9629)
      ],
      geocoder,
      suggest: true,
      routeWhileDragging: true
    }).addTo(this.map);

    // @ts-ignore
    const geoCoderControl = L.Control.geocoder({
      query: 'Moon',
      placeholder: 'Search here...',
      geocoder
    }).addTo(this.map);

    setTimeout(() => {
      geoCoderControl.setQuery('Earth');
    }, 12000);
    let marker;
    this.map.on('contextmenu', (e: any) => {
      // tslint:disable-next-line:one-variable-per-declaration
      const container = L.DomUtil.create('div');
      this.startBtn = this.createButton('Start Location', container);
      this.destBtn = this.createButton('End Location', container);
      geocoder.reverse(e.latlng, this.map.options.crs.scale(this.map.getZoom()), (results) => {
        const markerData = results[0];
        if (markerData) {
          if (marker) {
            marker
              .setLatLng(markerData.center)
              .setPopupContent(results[0].properties.display_name)
              .openPopup();
          } else {
            marker = L.marker(markerData.center)
              .bindPopup(markerData.name)
              .addTo(this.map)
              .openPopup();
          }
        }
      });
    });

    if (this.startBtn) {
      L.DomEvent.on(this.startBtn, 'click', (e: any) => {
        control.spliceWaypoints(0, 1, e.latlng);
        this.map.closePopup();
      });
    }
    if (this.destBtn) {
      L.DomEvent.on(this.destBtn, 'click', (e: any) => {
        control.spliceWaypoints(control.getWaypoints().length - 1, 1, e.latlng);
        this.map.closePopup();
      });
    }

  }

  createButton(label, container): HTMLElement {
    const btn = L.DomUtil.create('button', '', container);
    btn.setAttribute('type', 'button');
    btn.innerHTML = label;
    return btn;
  }

  ngOnInit(): void {
  }
}
