// typings for leaflet maps control
// this is needed as we are using different plugins of leafletjs to add on map

import * as L from 'leaflet';
declare module 'leaflet' {
  namespace control {
    function fullscreen(options?: any): any;
    function polylineMeasure(options?: any): any;
    function searchbox(options?: any): any;
    function locate(options?: any): any;
    function browserPrint(options?: any): any;
    function geocoder(options?: any): any;
  }
}
